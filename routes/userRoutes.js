const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");
const {verify} = auth;
const {verifyAdmin} = auth;

router.post('/register',userControllers.registerUser);
router.post('/log-in-authentication', userControllers.loginUser);
router.get('/log-in-successful', verify,userControllers.loginUserSuccess);
router.put('/setUserAsAdmin/:userId', verify,verifyAdmin,userControllers.setAsAdmin);


//para pagkuha lang ng id ng mga user
router.get('/',userControllers.allUser);


module.exports = router;
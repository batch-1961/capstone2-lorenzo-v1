const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");

const auth = require("../auth");
const {verify} = auth;
const {verifyAdmin} = auth;


//get active products access all user
router.get('/activeProduct', productControllers.getActiveProducts);

//retrieve single product
router.get('/singleProduct/:productId', productControllers.getSingleProduct);

// //create new product non admin
// router.post('/createProduct', productControllers.createProducts);

// create new product  admin
router.post('/createProductAdmin', verify,verifyAdmin,productControllers.createProductsAdmin);


//update product
router.put('/updateProduct/:productId', verify,verifyAdmin,productControllers.updateProducts);

//archive product
router.delete('/archiveProduct/:productId', verify, verifyAdmin,productControllers.archiveProducts);

//activate product
router.put('/activateProduct/:productId', verify, verifyAdmin,productControllers.activateProducts);

module.exports = router;
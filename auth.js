const jwt = require("jsonwebtoken");

const secret = "market";

module.exports.createAccessToken = (userDetails) => {

	// console.log(userDetails);
	const data = {
		id: userDetails.id,
		email: userDetails.email,
		isAdmin: userDetails.isAdmin
	}

	console.log(data);

	return jwt.sign(data,secret,{});

}


module.exports.verify = (req,res,next) => {

	let token = req.headers.authorization

	if(typeof token === "undefined"){
		return res.send({auth: "Please provide token."});
	} else {
		token = token.slice(7);
		
		jwt.verify(token,secret,function(err,decodedToken){
			if(err){
				return res.send({

				auth: "Please provide the correct token",
				message: err.message

				})
			} else {
				req.user = decodedToken;

				next();
			}
		})
	}
}

module.exports.verifyAdmin = (req,res,next) => {
	console.log(req.user);


	if(req.user.isAdmin){
		next();

	} else {
		return res.send({

			auth: "You are not an Admin",
			message: "Restricted"	

		})	
	}
}

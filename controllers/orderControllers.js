const Order = require("../models/Order");



module.exports.createOrder = (req,res) => {

	if(req.user.isAdmin){

	res.send({message: "You're an Admin, ordering not allowed!"})

	} else {

	let newOrder = new Order({
		userId: req.user.id,
		products: req.body.products,
		totalAmt: req.body.totalAmt

	})
	
	newOrder.save()
	.then(result => res.send({message: "Your order has been successfully placed!"}))
	.catch(error => res.send(error))
}
};



//get all orders by admin only

module.exports.getAllOrders = (req,res) => {

	Order.find()
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

//get user order by its user no input

module.exports.getUserOrder = (req,res) => {

	Order.find({userId: req.user.id}) //specific user lang
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


//get specific order of a user by its own user order no input di pa tapos
/*
module.exports.getSpecificUserOrder = (req,res) => {

	Order.findOne({_id:req.body.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}
*/

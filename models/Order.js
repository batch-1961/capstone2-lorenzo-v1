const mongoose = require("mongoose");


const orderSchema = new mongoose.Schema(
	{
		userId: {
		type:String,
		required: true
		},
		products: [
			{
				productId: {
					type: String
				},
				quantity: {
					type:Number,
					default: 1			
				},
				
			},
		],	
		totalAmt: {
			type: Number,
			required: true
			},

		purchasedOn: {
			type: Date,
			default: new Date()
		
		}

	
});

module.exports = mongoose.model("Order", orderSchema);
